To make these files into a working cartridge, create an app using the Cartridge Development Kit using this code as a base.

   rhc app create myphantomjscdk cdk --from-code https://gitlab.com/jaredjennings/openshift-phantomjs-cartridge.git

Then visit http://myphantomjscdk-myname.rhcloud.com to see the URL for the cartridge that you can add to your applications.

